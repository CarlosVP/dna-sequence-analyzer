import createHttpError from 'http-errors';
import mongoose, { mongo } from 'mongoose';
import { hasMutation } from '../../common/helpers';

interface IDna {
    dna_sequence: string[];
}

interface dnaModelInterface extends mongoose.Model<DnaDoc> {
    build(attr: IDna): DnaDoc;
}

interface DnaDoc extends mongoose.Document {
    _id: string;
    dna_sequence: string[];
    mutated: boolean;
}

const dnaSchema = new mongoose.Schema({
    _id: {
        type: String,
    },
    dna_sequence: {
        type: [
            {
                type: String,
                required: true,
                uppercase: true,
                validate: {
                    validator: (v: string) => {
                        return /^[ATCG]+$/.test(v) && v.length > 3;
                    },
                    message: props => `${props.value} is not a valid DNA dna_sequence.`,
                },
            },
        ],
        validate: {
            validator: (v: string[]) => {
                return !v.some(s => s.length !== v.length);
            },
            message: props => `${props.value} is not a valid NxN matrix.`,
        },
    },
    mutated: {
        type: Boolean,
        default: false,
    },
});

dnaSchema.statics.build = (attr: IDna) => {
    return new DNA(attr);
};

dnaSchema.pre('save', function (this: DnaDoc, next) {
    this._id = this.dna_sequence.join('');
    let exists = false;
    DNA.exists({ _id: this._id }, (err, doc) => {
        if (err) {
            console.log(err);
        } else {
            if (doc) exists = true;
        }
    });
    if (!exists) this.mutated = hasMutation(this.dna_sequence);
    next();
});

const DNA = mongoose.model<DnaDoc, dnaModelInterface>('Dna', dnaSchema);

export default DNA;
