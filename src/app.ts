import * as http from 'http';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import express from 'express';
import cors from 'cors';
import debug from 'debug';
import mongoose from 'mongoose';
import { CommonRoutesConfig } from './common/common.routes.config';
import { AnalyzerRoutes } from './analyzer/analyzer.routes.config';

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = process.env.PORT || 3000;
const routes: Array<CommonRoutesConfig> = [];
const debugLog: debug.IDebugger = debug('app');

app.use(express.json());

app.use(cors());

const loggerOptions: expressWinston.LoggerOptions = {
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.colorize({ all: true }),
    ),
};

if (!process.env.DEBUG) {
    loggerOptions.meta = false;
}

app.use(expressWinston.logger(loggerOptions));

routes.push(new AnalyzerRoutes(app));

const runningMessage = `Server running at http://localhost:${port} and connected to DB on dna_analyzer_db:27017/${process.env.MONGO_DB}`;
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(runningMessage);
});

mongoose
    .connect(
        `mongodb://${process.env.MONGO_USER}:${encodeURIComponent(process.env.MONGO_PASS!)}@dna_analyzer_db:27017/${
            process.env.MONGO_DB
        }?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false`,
        {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
        },
    )
    .then(() =>
        server.listen(port, () => {
            routes.forEach((route: CommonRoutesConfig) => {
                debugLog(`Routes configured for ${route.getName()}`);
            });
            console.log(runningMessage);
        }),
    )
    .catch(error => debugLog(`FATAL ERROR: ${error}`));
