import mongoose, { mongo } from 'mongoose';
import createHttpError from 'http-errors';

export const errorHandler = (err: any): createHttpError.HttpError => {
    if (err instanceof mongoose.Error.ValidationError) {
        for (const field in err.errors) {
            return createHttpError(400, err.errors[field]);
        }
        return createHttpError(500, 'We ran into some validation issues...');
    } else {
        if (err.code === 11000) return createHttpError(409, 'The resource already exists');
        return createHttpError(400, "Well...this is awkard, but we don't know what happened :/");
    }
};

// Since we're working with strings, to transpose the "matrix" we'll need to split each string in the array
// to separate each 'column' and add them to their transpose new strings.
const transpose = (array: string[]) => {
    const newArray: string[] = [];
    array.forEach(chain => {
        chain.split('').forEach((v, i) => (newArray[i] ? (newArray[i] += v) : newArray.push(v)));
    });
    return newArray;
};

// Traverse diagonally it's a little bit more tricky: we have to traverse from top left to bottom right
// and from bottom left to top right (or vice versa), and since we only care for four repeated characters
// in a sequence, this traverse must be from [n][3] to [n-2][n-3] and vice versa.
const diagonal = (array: string[], bottomToTop?: boolean) => {
    const Ylength = array.length;
    const Xlength = array[0].length;
    const maxLength = Math.max(Xlength, Ylength);
    let temp;
    const returnArray: string[] = [];
    for (let k = 3; k <= 2 * (maxLength - 1); ++k) {
        temp = [];
        for (let y = Ylength - 1; y >= 0; --y) {
            let x = k - (bottomToTop ? Ylength - y : y);
            if (x >= 0 && x < Xlength) {
                temp.push(array[y][x]);
            }
        }
        if (temp.length > 3) {
            returnArray.push(temp.join(''));
        }
    }
    return returnArray;
};

// Small regex test to validate if the string contains four characters repeated consecutively
const checkMutation = (chain: string) => /(.)\1{3}/.test(chain);

// In the worse scenario, the algo will reach the diagonal process, which would imply two nested loops twice
// in order to transform the matrix and also three calls to the some() method, which is not a loop internally
// since it works similarly to the map function()
export const hasMutation = (dna: string[]): boolean => {
    let mutatedH = false;
    let mutatedV = false;
    let mutatedD = false;
    // The some() method tests whether at least one element in the array passes the test implemented by the provided function.
    // It returns true if, in the array, it finds an element for which the provided function returns true; otherwise it returns false.
    // In the worse scenario, this will loop through the entire array (n), but in the best it'll loop just 1 time
    mutatedH = dna.some(checkMutation);
    // if there's no horizontal mutation, it'll evaluate the second array and so on until the algorithm reaches the diagonal dnas
    if (mutatedH) return mutatedH;
    else {
        const dnaV = transpose(dna);
        mutatedV = dnaV.some(checkMutation);
        if (mutatedV) return mutatedV;
        else {
            mutatedD = [...diagonal(dna), ...diagonal(dna, true)].some(checkMutation);
            if (mutatedD) return mutatedD;
        }
    }

    return mutatedH && mutatedV && mutatedD;
};
