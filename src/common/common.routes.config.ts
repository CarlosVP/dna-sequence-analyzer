import express from 'express';
export abstract class CommonRoutesConfig {
    app: express.Application;
    name: string;
    prefix?: string;

    constructor(app: express.Application, name: string, prefix?: string) {
        this.app = app;
        this.name = name;
        this.prefix = prefix;
        this.configureRoutes();
    }
    getName() {
        return this.name;
    }
    abstract configureRoutes(): express.Application;
}
