import { CommonRoutesConfig } from '../common/common.routes.config';
import DNA from '../db/models/DNA';
import { errorHandler } from '../common/helpers';
import express from 'express';

export class AnalyzerRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'AnalyzerRoutes');
    }

    configureRoutes() {
        this.app.route(`/stats`).get((req: express.Request, res: express.Response, next: express.NextFunction) => {
            // This is a special mongo feature (aggregations): Basically, these are a series of instructions to execute different operations
            // such as grouping, projecting (some sort of SELECT operation), and built-in functions like sum, conditionals, divisions, etc.
            // It's faster than requesting all data and then compute through it.
            DNA.aggregate([
                {
                    $group: {
                        _id: 'null',
                        count_mutations: {
                            $sum: {
                                $cond: ['$mutated', 1, 0],
                            },
                        },
                        count_no_mutations: {
                            $sum: {
                                $cond: ['$mutated', 0, 1],
                            },
                        },
                    },
                },
                {
                    $project: {
                        _id: 0,
                        count_mutations: '$count_mutations',
                        count_no_mutations: '$count_no_mutations',
                        ratio: {
                            $divide: ['$count_mutations', '$count_no_mutations'],
                        },
                    },
                },
            ]).exec((e, stats) => {
                if (e) {
                    const error = errorHandler(e);
                    return res.status(error.statusCode).send(error.message);
                }
                return res.status(200).send(stats.length ? stats[0] : 'Sorry, there are no records yet.');
            });
        });

        this.app
            .route(`/mutation`)
            .post(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                const { dna } = req.body;
                DNA.build({ dna_sequence: dna })
                    .save()
                    .then(data => {
                        return res.status(data.mutated ? 200 : 403).send(data);
                    })
                    .catch(e => {
                        const error = errorHandler(e);
                        return res.status(error.statusCode).send(error.message);
                    });
            });

        return this.app;
    }
}
