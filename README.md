# DNA Sequence Analyzer

> For testing, please import the [postman collection](dna-analyzer.postman_collection.json) in the root folder.
>
> > Disclaimer: the API is deployed on [AWS](http://ec2-18-191-24-155.us-east-2.compute.amazonaws.com/api/stats) but,
> > since I don't have the resources to add SSL certification, the app couldn't be deployed with HTTPS protocol enabled
> > and Chrome policies forbid accessing HTTP sites, the app only can be tested via Postman, cURL or other similar
> > tools.

This project uses the following stack:

-   TypeScript
-   Express.js
-   MongoDB
-   Docker & docker-compose

Other dependencies used are Mongoose, HTTP-Errors, and Winston. Also, there are other dev dependencies
used for consistency and standarize the application. For the whole list of dependencies, please take a look at the
package.json.

## Setup

> First at all, a `.env` file must be created with the required variables specified on [here](.env.example).

The project is dockerized, so I strongly recommend setting it up using containers instead of running the project
directly (since it depends on a DB service and the docker-compose scripts sets a MongoDB container up to avoid wiring up
another service). Anyways, the backend can be executed without containers, just by installing all dependencies, changing
the DB uri string on the [`app.ts`](src/app.ts) file on lines [45:46]. Then, just run `npm run start`.

If you don't have Docker installed, please follow these tutorials:

-   [Docker Engine Installation](https://docs.docker.com/engine/install/)
-   [Docker Compose Installation](https://docs.docker.com/compose/install/)

For creating the containers and setting all the necessary services up, just run the following command:

```bash
docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d
```

> This project has two different environments (local & production); in order to make local testings and later
> deployments to AWS, the compose files were split up. The `docker-compose.local.yml` includes all the logic to expose
> ports for local DB connection, debugging with Winston, and it mounts a volume into the container to refresh changes
> directly on the project.

## Functionality

The API can process NxN matrix where N > 3 (otherwise we couldn't detect mutations since these are 4-length). Any other
type of matrix will be rejected.

The implemented algorithm can be found inside `common` folder, in the [`helpers.ts`](src/common/helpers.ts)
